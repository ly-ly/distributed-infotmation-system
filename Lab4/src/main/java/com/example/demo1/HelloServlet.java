package com.example.demo1;

import java.io.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

//URl ссылка по которой можно попасть в данных сервлет
@WebServlet("/helloWorld")
//начальная страница
public class HelloServlet extends HttpServlet {
    //объявление полей класса
    private String message;
    //методы инициализации, присваиваем переменной message определенную строку
    @Override
    public void init() {
        message = "Hello Servlet!";
    }
    //метод отправки пользователю HTML страницы
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");

        PrintWriter out = response.getWriter();
        out.println("<html><body>");
        out.println("<h1>" + message + "</h1>");
        out.println("</body></html>");
    }
}