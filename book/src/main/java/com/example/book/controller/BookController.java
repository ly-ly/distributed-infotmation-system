package com.example.book.controller;

import com.example.book.domain.Book;
import com.example.book.repository.BookRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;


/**
 * The type Book controller.
 */
@Controller
@RequestMapping("/book")
public class BookController {

    private final BookRepository bookRepository;

    /**
     * Instantiates a new Book controller.
     *
     * @param bookRepository the book repository
     */
    @Autowired
    public BookController(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    /**
     * Gets all book.
     *
     * @param model the model
     * @return the all book
     */
    @GetMapping
    public String getAllBook(Model model) {
        model.addAttribute("books", bookRepository.findAll());
        return "book";
    }

    /**
     * Book profile string.
     *
     * @param book  the book
     * @param model the model
     * @return the string
     */
    @GetMapping("/{id}")
    public String bookProfile(@PathVariable("id") Book book,
                              Model model) {
        model.addAttribute("book", book);
        return "bookUpdate";
    }


    /**
     * Add book string.
     *
     * @param name       the name
     * @param author     the author
     * @param countPages the count pages
     * @param model      the model
     * @return the string
     */
    @PostMapping
    public String addBook(@RequestParam String name,
                          @RequestParam String author,
                          @RequestParam Integer countPages,
                          Model model) {
        Book book = new Book();
        book.setName(name);
        book.setAuthor(author);
        book.setCountPages(countPages);
        bookRepository.save(book);
        return "redirect:/book";
    }

    /**
     * Update book string.
     *
     * @param bookFromDB the book from db
     * @param name       the name
     * @param author     the author
     * @param countPages the count pages
     * @param model      the model
     * @return the string
     */
    @PostMapping("/{id}")
    public String updateBook(@PathVariable("id")Book bookFromDB,
                             @RequestParam String name,
                             @RequestParam String author,
                             @RequestParam Integer countPages,
                             Model model) {
        Book book = new Book();
        book.setName(name);
        book.setAuthor(author);
        book.setCountPages(countPages);
        BeanUtils.copyProperties(book, bookFromDB, "id");
        bookRepository.save(bookFromDB);
        return "redirect:/book";
    }

    /**
     * Delete book string.
     *
     * @param id the id
     * @return the string
     */
    @PostMapping("/delete/{id}")
    public String deleteBook(@PathVariable("id") Long id){
        bookRepository.deleteById(id);
        return "redirect:/book";
    }

}
