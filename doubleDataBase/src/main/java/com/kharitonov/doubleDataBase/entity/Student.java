package com.kharitonov.doubleDataBase.entity;

import lombok.Data;

import javax.persistence.*;

@Table(name = "student")
@Entity
@Data
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    private String surname;
    private String country;
    private Integer age;
}
