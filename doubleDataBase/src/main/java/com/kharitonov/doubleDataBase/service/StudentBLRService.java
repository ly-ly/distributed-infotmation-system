package com.kharitonov.doubleDataBase.service;

import com.kharitonov.doubleDataBase.entity.Student;
import com.kharitonov.doubleDataBase.repository.StudentBLRRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentBLRService {

    private StudentBLRRepository studentBLRRepository;

    @Autowired
    public StudentBLRService(StudentBLRRepository studentRepository) {
        this.studentBLRRepository = studentRepository;
    }

    public Student save(Student student) {
        return studentBLRRepository.save(student);
    }

    public List<Student> findAll() {
        return studentBLRRepository.findAll();
    }

    public void delete(Student student) {
        studentBLRRepository.delete(student);
    }

    public Student findById(Long id) {
        return studentBLRRepository.findById(id).get();
    }


}
