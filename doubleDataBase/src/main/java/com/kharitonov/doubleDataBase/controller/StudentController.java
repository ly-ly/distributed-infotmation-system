package com.kharitonov.doubleDataBase.controller;

import com.kharitonov.doubleDataBase.entity.Student;
import com.kharitonov.doubleDataBase.service.StudentBLRService;
import com.kharitonov.doubleDataBase.service.StudentOtherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.sql.Struct;
import java.util.ArrayList;
import java.util.List;

@RestController("/student")
public class StudentController {

    private StudentBLRService studentBLRService;
    private StudentOtherService studentOtherService;

    @Autowired
    public StudentController(StudentBLRService studentBLRService, StudentOtherService studentOtherService) {
        this.studentBLRService = studentBLRService;
        this.studentOtherService = studentOtherService;
    }

    @GetMapping
    public List<Student> getAllStudent() {
        ArrayList studentBLR = (ArrayList) studentBLRService.findAll();
        studentBLR.addAll(studentOtherService.findAll());
        return  studentBLR;
    }

    @PostMapping
    public Student saveStudent(@RequestBody Student student) {
        if(student.getCountry().equalsIgnoreCase("Беларусь")) {
            return studentBLRService.save(student);
        }
        return studentOtherService.save(student);
    }

}
