package com.kharitonov.doubleDataBase.controller;

import com.kharitonov.doubleDataBase.entity.Student;
import com.kharitonov.doubleDataBase.service.StudentBLRService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/studentblr")
public class StudentBLRController {

    private StudentBLRService studentBLRService;

    @Autowired
    public StudentBLRController(StudentBLRService studentBLRService) {
        this.studentBLRService = studentBLRService;
    }

    @GetMapping("/{id}")
    public Student getStudentById(@PathVariable("id") Long id) {
        return studentBLRService.findById(id);
    }

    @DeleteMapping("/{id}")
    public void deleteStudent(@PathVariable("id") Student student) {
        studentBLRService.delete(student);
    }
}
