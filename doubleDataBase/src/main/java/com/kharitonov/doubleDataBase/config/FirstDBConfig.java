package com.kharitonov.doubleDataBase.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(entityManagerFactoryRef = "studentBLREntityManagerFactory",
        transactionManagerRef = "studentBLRTransactionManager", basePackages = {"com.kharitonov.doubleDataBase.repository.StudentBLRRepository"})
public class FirstDBConfig {

    @Primary
    @Bean(name = "firstDataSource")
    @ConfigurationProperties(prefix="first.datasource")
    public DataSource dataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean(name = "studentBLREntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean studentBLREntityManagerFactory(
            EntityManagerFactoryBuilder builder, @Qualifier("firstDataSource") DataSource dataSource) {
        return builder.dataSource(dataSource)
                .packages("com.kharitonov.doubleDataBase.entity")
                .persistenceUnit("")
                .build();
    }

    @Bean(name = "studentBLRTransactionManager")
    public PlatformTransactionManager studentBLRTransactionManager(
            @Qualifier("studentBLREntityManagerFactory") EntityManagerFactory studentBLREntityManagerFactory) {
        return new JpaTransactionManager(studentBLREntityManagerFactory);
    }
}
