package com.kharitonov.doubleDataBase.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(entityManagerFactoryRef = "studentOtherEntityManagerFactory",
        transactionManagerRef = "studentOtherTransactionManager", basePackages = {"com.kharitonov.doubleDataBase.repository.StudentOtherRepository"})
public class SecondDBConfig {

    @Primary
    @Bean(name = "secondDataSource")
    @ConfigurationProperties(prefix="second.datasource")
    public DataSource dataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean(name = "studentOtherEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean studentOtherEntityManagerFactory(
            EntityManagerFactoryBuilder builder, @Qualifier("secondDataSource") DataSource dataSource) {
        return builder.dataSource(dataSource)
                .packages("com.kharitonov.doubleDataBase.entity")
                .persistenceUnit("")
                .build();
    }

    @Bean(name = "studentOtherTransactionManager")
    public PlatformTransactionManager studentOtherTransactionManager(
            @Qualifier("studentOtherTransactionManager") EntityManagerFactory studentOtherTransactionManager) {
        return new JpaTransactionManager(studentOtherTransactionManager);
    }
}
